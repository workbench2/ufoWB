
import wbBase

__version__ = "0.1.0"

class App(wbBase.application.App):
    """
    UFO WorkBench application.

    Each WorkBench application needs its own application class in order to  
    allow the python importlib to find application resources and plugins.
    """

