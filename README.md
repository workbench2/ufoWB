# ufoWB

## UFO Workbench

Font editor for font source files in UFO format.

## Installation

```shell
pip install ufoWB
```

If you intend to write your own Python scripts for the FontTools Workbench, 
you may want to install additional plugins to aid in development. 
This can be done as follows:
```shell
pip install ufoWB[develop]
```

After installation you find an executable in the usual location:
- On Windows:
    
    `C:\Python311\Scripts\ufoWB.exe`

- On Mac:

    `Lib/Frameworks/Python.framework/Versions/3.11/bin/ufoWB`

The actual executable path may vary depending on your Python version 
and installation location.

Launch the executable to run the application.
## Documentation

For details read the [Documentation](https://workbench2.gitlab.io/ufoWB/).
